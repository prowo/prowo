#!/usr/bin/python
#coding=utf-8
import socket
import sys
import json

def client(ip,port,message):

    s = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    s.connect((ip,port))
    try:
        print("Send:"+message)
        s.sendall(message.encode())
        response = s.recv(1024).decode()
        jresp = json.loads(response)
        print("Recv:"+str(jresp))

    finally:
        s.close()

def run(GWid, EQid):
    HOST, PORT = '182.254.134.84', 22223
    msg1 = {'GWid':GWid, 'EQid':EQid,'token':'hahah'}
    
    jmsg1 = json.dumps(msg1)
    client(HOST,PORT,jmsg1)
if __name__ == "__main__":
    HOST,PORT = "182.254.134.84",22223
    
    msg1 = {'GWid':'0x0101','EQid':'switchv2.0','token':'dhahah'}
    jmsg1 = json.dumps(msg1)
    client(HOST,PORT,jmsg1)
