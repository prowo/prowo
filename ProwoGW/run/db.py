import pymysql
import configparser

config = configparser.ConfigParser()
config.read('db.ini')
host = config.get("Settings","host")
port = config.get("Settings","port")
user = config.get("Settings","user")
passwd = config.get("Settings","passwd")
db = config.get("Settings","db")
charset = config.get("Settings","charset")

def exec(sql):
    """
    """
    conn = pymysql.connect(host=host, port=int(port), user=user, passwd=passwd,db=db,charset=charset)
    cursor = conn.cursor()
    result =cursor.execute(sql)
    conn.commit()
    cursor.close()
    conn.close()

def get(sql):
    """
    """
    conn = pymysql.connect(host=host, port=int(port), user=user, passwd=passwd,db=db,charset=charset)
    cursor = conn.cursor()
    cursor.execute(sql)
    result = cursor.fetchone()
    if result!=None:
        result = result[0]
    cursor.close()
    conn.close()
    return result

if __name__ == "__main__":
    print(get_port())


