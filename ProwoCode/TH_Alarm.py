import time

from prowo import Device
from prowo import Prowo

startT = time.time()
# 创建Prowo连接
p = Prowo('192.168.12.1', 'Vudo3423', 'host')

# 实例化警报器
alarm = Device(p, 'switch001')
# 实例化温传
dht11 = Device(p, 'dht11v21.0')

while True:
    temperature = dht11.value()[0]
    print(temperature)
    if  temperature > 20.0:
        # 启动警报
        alarm.do('on')
        time.sleep(2)
        alarm.do('off')
        break
    else:
        alarm.do('off')
    
    runT = time.time()
    if runT - startT > 60:
        break
